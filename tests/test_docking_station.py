import pytest

from app.bike import Bike
from app.docking_station import DockingStation
from app.errors import NoBikesError, StationFullError


def test_docking_station_initializes_with_empty_bikes(docking_station):
    assert docking_station.bikes == []


def test_docking_station_initializes_with_a_capacity_of_twenty(docking_station):
    assert docking_station.capacity == DockingStation.DEFAULT_CAPACITY


def test_docking_station_can_be_initialized_with_custom_capacity():
    docking_station = DockingStation(capacity=5)
    assert docking_station.capacity == 5


def test_docking_station_release_bike_releases_a_bike_with_a_bike_available(mocker, docking_station):
    bike_mock = mocker.Mock()
    docking_station.dock(bike_mock)

    assert docking_station.release_bike() == bike_mock
    assert not docking_station.bikes


def test_docking_station_release_bike_raises_an_error_when_no_working_bikes(mocker, docking_station):
    broken_bike = mocker.Mock(Bike, is_working=False)
    docking_station.dock(broken_bike)

    with pytest.raises(NoBikesError):
        docking_station.release_bike()


def test_docking_station_release_bike_raises_an_error_when_no_bikes_available(docking_station):
    with pytest.raises(NoBikesError):
        docking_station.release_bike()


def test_docking_station_dock_stores_a_bike(mocker, docking_station):
    bike_mock = mocker.Mock()
    docking_station.dock(bike_mock)
    assert bike_mock in docking_station.bikes


def test_docking_station_dock_stores_a_broken_bike(mocker, docking_station):
    broken_bike_mock = mocker.Mock()
    docking_station.dock(broken_bike_mock)
    assert broken_bike_mock in docking_station.bikes


def test_docking_station_stores_multiple_bikes(mocker, docking_station):
    bike_mock = mocker.Mock()

    for i in range(5):
        docking_station.dock(bike_mock)

    assert len(docking_station.bikes) == 5


def test_docking_station_dock_raises_error_when_full(mocker, docking_station):
    bike_mock = mocker.Mock()

    for i in range(DockingStation.DEFAULT_CAPACITY):
        docking_station.dock(bike_mock)

    with pytest.raises(StationFullError):
        docking_station.dock(bike_mock)
