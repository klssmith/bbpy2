import pytest

from app.bike import Bike
from app.docking_station import DockingStation
from app.errors import NoBikesError, StationFullError


def test_docking_station_releases_a_bike(docking_station, bike):
    docking_station.dock(bike)
    released_bike = docking_station.release_bike()
    assert released_bike == bike


def test_docking_station_releases_a_working_bike(docking_station, bike):
    docking_station.dock(bike)
    released_bike = docking_station.release_bike()
    assert released_bike.is_working


def test_docking_station_does_not_release_broken_bikes(docking_station, broken_bike):
    bike1 = Bike()
    bike2 = Bike()

    docking_station.dock(broken_bike)
    docking_station.dock(bike1)
    docking_station.dock(bike2)

    docking_station.release_bike()
    docking_station.release_bike()

    with pytest.raises(NoBikesError):
        docking_station.release_bike()

    assert len(docking_station.bikes) == 1
    assert docking_station.bikes == [broken_bike]


def test_empty_docking_stations_do_not_release_bikes(docking_station, bike):
    with pytest.raises(NoBikesError):
        docking_station.release_bike()


def test_dock_bike_at_docking_station(docking_station, bike):
    docking_station.dock(bike)
    assert docking_station.bikes == [bike]


def test_docking_stations_do_not_accept_bikes_when_full(docking_station, bike):
    for i in range(DockingStation.DEFAULT_CAPACITY):
        docking_station.dock(bike)

    with pytest.raises(StationFullError):
        docking_station.dock(bike)


@pytest.mark.skip
def test_van_collects_broken_bikes(bike, broken_bike, docking_station):
    van = Van()

    docking_station.dock(bike)
    docking_station.dock(broken_bike)

    van.collect_broken_bikes(docking_station)

    assert docking_station.bikes == [bike]
    assert van.bikes == [bike]
