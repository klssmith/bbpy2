def test_bike_initializes_working(bike):
    assert bike.is_working


def test_bike_report_broken_changes_the_status_of_a_bike(bike):
    bike.report_broken()
    assert not bike.is_working
