import pytest

from app.bike import Bike
from app.docking_station import DockingStation
from app.van import Van


@pytest.fixture
def docking_station():
    """Returns a Docking Station with default capacity"""
    return DockingStation()


@pytest.fixture
def bike():
    """Returns a working Bike"""
    return Bike()


@pytest.fixture
def broken_bike():
    """Returns a broken Bike"""
    bike = Bike()
    bike.report_broken()
    return bike


@pytest.fixture
def van():
    """Returns a Van"""
    return Van()
