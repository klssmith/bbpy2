from app.bike import Bike
from app.docking_station import DockingStation


def test_van_initializes_with_empty_bikes(van):
    assert van.bikes == []


# def collect_broken_bikes(self, location):
#     # collected_bikes is a list of bikes
#     collected_bikes = location.release_bikes()
#     self.bikes.extend(collected_bikes)

import pdb
def test_van_collect_broken_bikes_stores_broken_bikes(mocker, van):
    broken_bike = mocker.Mock(Bike, is_working=False)
    docking_station = mocker.Mock(DockingStation, release_bikes=[broken_bike])

    pdb.set_trace()
    # mocker.patch(
    #     'tests.test_van.docking_station.release_bikes',
    #     return_value=[broken_bike]
    # )

    mocker.patch(
        'app.docking_station.release_bikes',
        return_value=[broken_bike]
    )

    van.collect_broken_bikes(docking_station)

    assert broken_bike in van.bikes
