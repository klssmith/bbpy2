from app.errors import NoBikesError, StationFullError


class DockingStation:
    DEFAULT_CAPACITY = 20

    def __init__(self, capacity=DEFAULT_CAPACITY):
        self._bikes = []
        self.capacity = capacity

    @property
    def bikes(self):
        return self._bikes

    def release_bike(self):
        if self._any_working_bikes():
            for i in range(len(self.bikes)):
                current_bike = self.bikes[i]

                if current_bike.is_working:
                    self.bikes.remove(current_bike)
                    return current_bike

        raise NoBikesError

    def dock(self, bike):
        if self._is_full():
            raise StationFullError
        else:
            self.bikes.append(bike)

    def _is_full(self):
        return len(self.bikes) >= self.capacity

    def _any_working_bikes(self):
        return any(bike.is_working for bike in self.bikes)
