class Van():
    def __init__(self):
        self._bikes = []

    @property
    def bikes(self):
        return self._bikes

    def collect_broken_bikes(self, location):
        # collected_bikes is a list of bikes
        collected_bikes = location.release_bikes()
        self.bikes.extend(collected_bikes)


"""
The process of collecting bikes (let's ignore their working status for now...)

Van initiates the process (because this is where the bikes end up)

And because it can collect from multiple places, it has to specify where to collect from

So it has to call a method which takes a location


That method has to kick off a process in the docking station (or whatever)

The responsibility of the docking station is to release the bikes, which it does by
- returning the 'correct' bikes
- and removing them from it's storage

Van then takes the bikes returned from the docking station and stores them

"""
