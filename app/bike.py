class Bike:
    def __init__(self):
        self._is_working = True

    @property
    def is_working(self):
        return self._is_working

    def report_broken(self):
        self._is_working = False
