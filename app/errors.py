class NoBikesError(Exception):
    def __init__(self):
        self.message = 'There are no bikes available'


class StationFullError(Exception):
    def __init__(self):
        self.message = 'The docking station is full'
